using Application;
using System;

namespace Ontwikkelopdracht
{
    public class UserInput
    {
        public static MatrixEntries ReceiveMatrixEntries(int sizeOfMatrix)
        {
            var matrixEntries = new MatrixEntries();
            for (int rowNumber = 0; rowNumber < sizeOfMatrix; rowNumber += 1)
            {
                for (int columnNumber = 0; columnNumber < sizeOfMatrix; columnNumber += 1)
                {
                    Console.WriteLine($"Entry for row {rowNumber + 1}, column {columnNumber + 1}:");
                    string entry = Console.ReadLine();
                    int value = int.Parse(entry);
                    var newEntry = new MatrixEntry(rowNumber, columnNumber, value);
                    matrixEntries.Add(newEntry);
                }
            }
            return matrixEntries;
        }

        public static int ReceiveMatrixSize()
        {
            Console.WriteLine("Input the size of the matrix:");
            return int.Parse(Console.ReadLine());
        }
    }
}