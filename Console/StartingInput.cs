﻿using Application;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ontwikkelopdracht
{
    public class StartingInput
    {
        private static int size = 4;
        private static List<int> ValuesForMatrixEntries = new List<int>();

        public static void ParseStartingInput(string[] arguments)
        {
            for (int i = 0; i < arguments.Length; i++)
            {
                switch (arguments[i])
                {
                    case "--size":
                        ParseSize(arguments[i+1]);
                        break;
                    case "--entries":
                        ParseMatrixEntryValues(arguments[i+1]);
                        break;
                }
            }
        }

        public static void ParseMatrixEntryValues(string ValuesInput)
        {
            ValuesForMatrixEntries = ValuesInput.Split(',')
                                    .Select(Int32.Parse)
                                    .ToList();
        }

        public static void ParseSize(string matrixSize)
        {
            size = int.Parse(matrixSize);
        }

        internal static int DetermineSize()
        {
            return size;
        }

        public static MatrixEntries DetermineMatrixEntries()
        {
            var entries = new MatrixEntries();
            for (int i = 0; i < size * size; i++)
            {
                int row = i / size;
                int column = i % size;
                int value = 0;
                if (i < ValuesForMatrixEntries.Count)
                {
                    value = ValuesForMatrixEntries[i];
                }
                var entry = new MatrixEntry(row, column, value);
                entries.Add(entry);
            }
            return entries;
        }
    }
}
