﻿using Application;
using System;

namespace Ontwikkelopdracht
{
    class Program
    {
        static void Main(string[] args)
        {
            var userGeneratedMatrix = new Matrix();
            if (args.Length != 0)
            {
                StartingInput.ParseStartingInput(args);
                int size = StartingInput.DetermineSize();
                MatrixEntries entries = StartingInput.DetermineMatrixEntries();
                userGeneratedMatrix = new Matrix(size, entries);
            }
            else
            {
                int size = UserInput.ReceiveMatrixSize();
                MatrixEntries matrixEntries = UserInput.ReceiveMatrixEntries(size);
                userGeneratedMatrix = new Matrix(size, matrixEntries);
            }
            PrintMatrix(userGeneratedMatrix);
            userGeneratedMatrix.CalculateAbsoluteDifferenceInDiagonals();
            PrintMatrixProperties(userGeneratedMatrix);
            Console.WriteLine("done");
            Console.ReadKey();
        }

        private static void PrintMatrixProperties(Matrix userGeneratedMatrix)
        {
            Console.WriteLine($"The absolute difference of the two diagonals of the matrix is: {userGeneratedMatrix.AbsoluteDifferenceInDiagonals}");
            Console.WriteLine($"The sum over column 3 is: {userGeneratedMatrix.SumOverColumn(2)}");
            Console.WriteLine($"The sum over row 3 is: {userGeneratedMatrix.SumOverRow(2)}");
        }

        public static void PrintMatrix(Matrix matrix)
        {
            var valuesByRow = matrix.FindValuesByRow();
            Console.WriteLine("The matrix we use is:");
            foreach(var valuesInRow in valuesByRow)
            {
                Console.WriteLine(string.Join(" ", valuesInRow));
            }
        }
    }
}
