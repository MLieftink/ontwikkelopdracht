﻿namespace Application
{
    public class MatrixEntry
    {
        public int Row { get; private set; }
        public int Column { get; private set; }
        public int Value { get; set; }

        public MatrixEntry(int row, int column, int value)
        {
            Row = row;
            Column = column;
            Value = value;
        }
    }
}
