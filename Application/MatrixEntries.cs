﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Application
{
    public class MatrixEntries : List<MatrixEntry>
    {

        public int SumOf(Func<MatrixEntry, bool> predicate)
        {
            return this
                .Where(predicate)
                .Sum(i => i.Value);
        }

        public List<int> FindValuesOf(Func<MatrixEntry, bool> predicate)
        {
            return this
                .Where(predicate)
                .Select(i => i.Value)
                .ToList();
        }
    }
}