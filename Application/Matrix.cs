using System;
using System.Collections.Generic;
using System.Linq;

namespace Application
{
    public class Matrix
    {
        private MatrixEntries entries;

        private int size;
        public int AbsoluteDifferenceInDiagonals { get; private set; }

        public Matrix(int size, MatrixEntries entries)
        {
            this.size = size;
            this.entries = entries;
        }
        public Matrix() { }

        public void CalculateAbsoluteDifferenceInDiagonals()
        {
            int leftToRightDiagonal = entries.SumOf(x => x.Row == x.Column);
            int rightToLeftDiagonal = entries.SumOf(x => x.Row + x.Column == size - 1);

            AbsoluteDifferenceInDiagonals = Math.Abs(leftToRightDiagonal - rightToLeftDiagonal);
        }

        public int SumOverRow(int row)
        {
            return entries.SumOf(x => x.Row == row);
        }

        public int SumOverColumn(int column)
        {
            return entries.SumOf(x => x.Column == column);
        }
        public int SumOverAll()
        {
            return entries.SumOf((i) => true);
        }

        public IEnumerable<IEnumerable<int>> FindValuesByRow()
        {
            var valuesInRows = new List<List<int>>();
            for(int row = 0; row < size; row ++ )
            {
                var valuesInRow = entries.FindValuesOf(x => x.Row == row);
                valuesInRows.Add(valuesInRow);
            }
            return valuesInRows;
        }
    }

}